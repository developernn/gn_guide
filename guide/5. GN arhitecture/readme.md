# GN arhitecture


### Build structure.
2.png
executable(“doom_melon”) {
  print(configs)
  ...
}
> gn gen out/Default

["//build/config:feature_flags",
"//build/config/compiler:compiler",
"//build/config/compiler:clang_stackrealign",
"//build/config/compiler:compiler_arm_fpu",
"//build/config/compiler:chromium_code",
"//build/config/compiler:default_include_dirs",
"//build/config/compiler:default_optimization", "//build/config/compiler:default_symbols",
"//build/config/compiler:no_rtti",
"//build/config/compiler:runtime_library",
"//build/config/sanitizers:default_sanitizer_flags",
"//build/config/sanitizers:default_sanitizer_coverage_flags",
"//build/config/win:lean_and_mean",
"//build/config/win:nominmax",
"//build/config/win:unicode",
"//build/config/win:winver",
"//build/config:debug"]



files and how they used


### Toolchains
Imagine your build as an n-dimensional hypercube...
2.png
3.png

What’s a toolchain?
* Identified by a label
* Defines a set of compiler and linker rules.
* Goes with a set of variables (OS, CPU, etc.)

* Cross-toolchain dependencies.
executable(“chrome”) {
  … 
  data_deps = [
   “//nacl:irt(//build/toolchain/nacl:newlib)”
  ]
}

action(“compile_some_protos”) {
  … 
  deps = [
    “:proto_compiler($host_toolchain)”
  ]
}


* Comparing toolchains.
if (current_toolchain ==
    host_toolchain) {
  executable(“proto_compiler”) {
    …
  }
}

* Generate projects for popular IDEs
see “gn help gen”



### Important files
* .gn
- Defines root of GN build tree.
- See “gn help dotfile”
* //build/config/BUILDCONFIG.gn
Exact location defined by “.gn” file
Sets up global variables and default settings

The directory where .gn is located will be identified by the GN tool as the source root of the project. The content of .gn is basically to use buildconfig to specify the location of the build config, where //build/BUILDCONFIG.gn is used to specify the path relative to the source root.



https://www.topcoder.com/thrive/articles/Introduction%20to%20Build%20Tools%20GN%20&%20Ninja - вот тут описание некоторых фич из файла [Step 1 - Step 3)