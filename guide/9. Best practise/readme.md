# Best practise


### How should you design your build?
- Design your build like code:
* Modular
GN small targets and lots of directories!
* Clear relationship between modules

- Protect your code from your team.
* deps vs. public_deps — control how you expose your dependencies
* visibility — limit what can depend on you
* assert_no_deps — “none of my dependencies should link Blink”
* testonly — can’t be linked into production code
* List public headers in “public” — other headers become “private”





