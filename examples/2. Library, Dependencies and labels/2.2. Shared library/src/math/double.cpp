#include "double.h"

namespace math
{
    int fdouble(int value)
    {
        return value * value;
    }
} // namespace math