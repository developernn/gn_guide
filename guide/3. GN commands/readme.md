# GN commands

For get help
```
gn help
```

This commands outputs a lot of things:
* Commands
* Target declarations
* Buildfile functions
* Built-in predefined variables
* Variables you set in targets
* Other help topics

In this section we will intrestend in commands. Other topics we will see in "GN language".

I will cover all u need. If u dont find answer u can also use "help" command. Also u can get more detail about scpeific topic:
```
gn help <variable | function | command>
```


## Commands
There is only 1% of possibilities. I try to show the most used commands by me. When u have certain task u just use 'help'

Path can be project absolute (//), system absolute (/) or relative.

Remeber everything u need probably in help. Also firstly before do something by yourself, for example create script to parse something, may be there already flag in tool which would help.


### gn gen
* Generate ninja files
```
gn gen <out_dir>
```
Generates ninja files from the current tree and puts them in the given output directory.

U need make output directory once. If u change something ninja files will be regenerated. Approvement:
touch BUILD.gn
ninja -C out

* Dumps target information and their dependencies to a JSON file.
```
gn gen --ide="json" --filters="//:hello"
```
Only matching targets  will be included in the solution.

* Generate compilation database
```
gn gen --export-compile-commands --add-export-compile-commands="//src/*" out
```
Adds an additional label pattern of a target to add to the compilation database. 
Now there a new way to do it. This is deprecated but works.


### gn ls
* List targets for the given build directory
```
gn ls out
```


### gn desc
* Show lots of insightful information about a target or config
```
gn desc out //:hello
```


### gn path
* Finds paths of dependencies between two targets.
```
gn path <out_dir> <target_one> <target_two>
```
Find paths between two targets. How do I depend on that? Why can’t I use a header from a dependency?



### gn refs
* Find stuff referencing a target or file.
```
gn refs out <target>
```


### gn clean
* Cleans the output directory and creates a Ninja build environment sufficient to regenerate the build
```
gn clean out
```


### gn args
Lists all build arguments available in the current configuration (only the names and current values)
```
gn args out --list --short
```

### gn outputs
* Lists the output files corresponding to the given target(s) or file name(s).
```
gn outputs out //:test
gn outputs out src/main.cpp
```

* Compiles all files changed in git.
```
git diff --name-only | xargs gn outputs out/x64 | xargs ninja -C out/x64
```   






??????????????? meta: List target metadata collection results.


