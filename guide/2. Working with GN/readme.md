# Working with GN

Afther we understand what is GN and Ninja are and what part they take in two-part build system, we can go further and understand how to work with this.


### Worflow
Afther all tools is installed (gn and ninja) we can start working. 

* Project is set up
1. Generate .ninja files using gn
```
// Syntax:
gn gen <output_folder>
// Examples:
gn gen out
```

2. Use ninja to build
```
// Syntax:
ninja -C <folder with ninja build scripts>
// Examples:
ninja -C out
```

3. Start app


* New project
This section is about how to add GN to basic "hello world" project.

0. Create project folder

1. Download build folder from official report and copy to your project source folder.
I found this folder in examples/simple_build. 

2. Add a .gn to the root directory of the source project
```
buildconfig = "//build/BUILDCONFIG.gn"
```

3. Create BUILD.gn to the source root
```
executable("hello") {
  sources = [
    "src/main.cpp",
  ]
}
```

4. Generate ninja build scripts
```
gn gen out
```

5. Build project
```
ninja -C out
```

6. App is ready 
```
./hello
```

Example (examples/1. hello gn)


* Change
if source code:
```
ninja -C out
```
if gn files - no need to do anything.


















