# GN language
Let's explore what we can write using gn language. All this for BUILD.gn files, which is ....


## Libraries, dependencies and labels
### Libraries
* Static library
static_library(“base”) {
  sources = [
    “a.cc”,
    “b.cc”,
  ]
}

* Shared library
shared_library("math") {
  sources = [
    "double.cpp"
  ]
}

Also this will not work. We need specify dependencies:

### Dependencies
* 
static_library(“base”) {
  sources = [
    “a.cc”,
    “b.cc”,
  ]
  deps = [
    “//fancypants”,
    “//foo/bar:baz”,
  ]
}

To understand why deps name look like this we need understand labels:

### Labels
* Labels
- Full label
```
//chrome/browser:version
```
→ Looks for “version” in chrome/browser/BUILD.gn

- Implicit name
```
//base
```
→ Shorthand for //base:base Useful when a folder has a “main thing”.

- In current file
```
:baz
```
→ Shorthand for “baz” in current file.


Example 2 folder



## Control flow
* Conditionals and expressions
component(“base”) {
  sources = [
    “a.cc”,
    “b.cc”,
  ]

  if (is_win || is_linux) {
    sources += [ “win_helper.cc” ]
  } else {
    sources -= [ “a.cc” ]
  }
}

// what is component? это походу какая-то кастомная хуйня из chrome
Example 3. control flow


## Targets


### Built-in target types
executable, shared_library, static_library
loadable_module: like a shared library but loaded at runtime
source_set: compiles source files with no intermediate library
group: a named group of targets (deps but no sources)
copy
action, action _foreach: run a script
bundle_data, create_bundle: Mac & iOS


### Common Chrome-defined ones
component: shared library or source set depending on mode
test 
app: executable or iOS application + bundle
android_apk, generate_jni, etc.: Lots of Android ones!



## Configuration

### Compiler configuration
executable(“doom_melon”) {
  sources = [ “doom_melon.cc” ]

  cflags = [ “-Wall” ]
  defines = [ “EVIL_BIT=1” ]
  include_dirs = [ “.” ]

  deps = [ “//base” ]
}

### Configs group flags with a name.
config(“myconfig”) {
  defines = [ “EVIL_BIT=1” ]
}
executable(“doom_melon”) {
  ...
  configs += [ “:myconfig” ]
}
test(“doom_melon_tests”) {
  ...
  configs += [ “:myconfig” ]
}

### Apply settings to targets that depend on you.
config(“icu_dirs”) {
  include_dirs = [ “include” ]
}

shared_library(“icu”) {
  public_configs = [ “:icu_dirs” ]
}

executable(“doom_melon”) {
  deps = [
    # Apply ICU’s public_configs.
    “:icu”,
  ]
}

### Forward public configs up the dependency chain.
shared_library(“i18n_utils”) {
  …
  public_deps = [
    “//third_party/icu”,
  ]
}

executable(“doom_melon”) {
  deps = [
    # Apply ICU’s public_configs.
    “:i18n_utils”,
  ]
}

### A target can modify the configs to opt-out of defaults.
executable(“doom_melon”) {
  configs -= [
    "//build/config/compiler:chromium_code",
  ]
  configs += [
    "//build/config/compiler:no_chromium_code",
  ]
}


## Arguments

### Declate arguments
Это типо определять аргументы иначе не будет работать? например без этой функции? или просто default значения типа которые мы можем прописать файле чтобы их не передавать?
А тип это дока юудет показано когда параметры выводим?
declare_args() {
  # Documentation
  # Allow unlimited requests
  # to the Google speech API.
  bypass_speech_api_quota = false 
  # Arg name = Default value
}
executable(“doom_melon”) {
  if (bypass_speech_api_quota) {
    …
  }
}
```
> gn args out/Default
```
bypass_speech_api_quota = true
is_debug = false
is_component_build = true

Думая для моей доке нужно добавить описание что делает args типо открывает хуйню и заставляет перегенить ninja

gn args --list out/Default



## Variables

### Built-in variables


### Shared variables are put in a *.gni file and imported.
// gni
declare_args() {
  # Controls Chrome branding.
  is_chrome_branded = false
}
enable_crashing = is_win
// buildcoinfig.
import(“//foo/build.gni”)
executable(“doom_melon”) {
  if (is_chrome_branded) {
    …
  }
  if (enable_crashing) {
    … 
  }
}
В чем разница между этими двумя переменными? обязательнон ужно импортить? как все нахуй работает


## Templates
Templates allow creating of new target types.
template(“grit”) {
  …
}
grit(“components_strings”) {
  source = “components.grd”
  outputs = [ … ]
}


## Actions
* Actions run Python scripts.
action(“myaction”) {
  script = “myscript.py”

* Dependency management.
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    …
  ]
This writes a file to the source tree!
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    “generated.txt”,  # Error!
  ]

Put outputs in the target-specific out directory.
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    target_out_dir + “/output.txt”,
  ]
  print(outputs)
> gn gen out/Default
[“//out/Default/obj/foo/output.txt”]

* Use $foo or ${foo} to expand variables in strings.
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    “$target_out_dir/output.txt”,
  ]
  print(“out = $outputs”)
> gn gen out/Default
out = [“//out/Default/obj/foo/output.txt”]

* Args are what is passed to the script.
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    “$target_out_dir/output.txt”,
  ]
  args = [
    “-i”, inputs[0], outputs[0],
  ]
}
>>> ERROR can’t open “myfile.txt”
or “//out/Default/obj/output.txt”

* The script working directory is root_build_dir
action(“myaction”) {
  script = “myscript.py”
  inputs = [ “myfile.txt” ]
  outputs = [
    “$target_out_dir/output.txt”,
  ]
  args = [
    “-i”,
    rebase_path(inputs[0],
                root_build_dir)
    rebase_path(outputs[0],
                root_build_dir)
  ]
}

* action_foreach runs a script over each source.
action_foreach(“process_idl”) {
  script = “idl_compiler.py”
  inputs = [ “static_input.txt” ]
  sources = [
    “a.idl”,
    “b.idl”,
  ]

* Magic substitutions for dealing with multiple sources.
action_foreach(“process_idl”) {
  script = “idl_compiler.py”
  inputs = [ “static_input.txt” ]
  sources = [
    “a.idl”,
    “b.idl”,
  ]
  outputs = [
    “$target_gen_dir/{{source_name_part}}.h”
  ]
  args = [
    “--input={{source}}”
  ]
}


### Other
```
template(“grit”) {
  # Magic target_name variable expands to “components_strings” in this example.
  action(target_name) {
    script = “//tools/grit.py”
    # Access the variables from the caller via “invoker.”
    sources = [ invoker.source ]
    ...
  }
}
grit(“components_strings”) {
  source = “components.grd”
  outputs = [ … ]
}
```

* exec_script: The universal escape hatch.
gypi_values = exec_script(
  “//build/gypi_to_gn.py”,
  [ rebase_path(“chrome_browser.gypi”) ],
  “scope”,
  [ “chrome_browser.gypi” ])






Все что сверху еще gn help

А как лучше типо в каждой папке свой BUILD.gn и до него путь прописываем? типа такая логика. 

* Target declarations
* Buildfile functions
* Built-in predefined variables
* Variables you set in targets
* Other help topics



Вот эта хуйня вообще не понятная:
### Some things the code loads dynamically.
test(“doom_melon_tests”) {
  # This file is loaded @ runtime.
  data = [
    “melon_cache.txt”,
  ]
}


shared_library(“icu”) {
  # This target is loaded @ runtime.
  data_deps = [
    “:icu_data_tables”,
  ]
}