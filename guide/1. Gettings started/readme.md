# Getting started

This guide is first part of "GN & ninja" buildtools guide. Here we will talk about GN.

Idea of of two part system GN & ninja is that GN is meta build system which generate .ninja files. And ninja which is counterpart.
When .ninja scripts is generated, ninja takes this .ninja files and source code and generate binary. Whole responsobility goes to ninja. Then ninja use its configuration and use compliler to produce corresponding output.

All u need to understand in this guide about Ninja: it is build system. In second guide (https://ninja-build.org/) we will watch at ninja.



### What is GN?
GN is a meta-build system that generates Ninja Build files (build files for Ninja).

By meta-build system means build system which produce files to anorther build system. 
GN is meta-build system, because it takes .gn build scripts and translate then .ninja scripts:
1. png


### History of GN
It is a new tool developed in Google for fast builds of projects. It is open-sourced. 

They developed it to replace GYP (https://gyp.gsrc.io/). GN is written in C++, it is 20 times faster than GYP written in Python.

GN is currently used as the build system for Chromium, Fuchsia.


### Purpose of GN
Generate Ninja files (.ninja).

It outputs only Ninja build files.

U need understand that GN generate .ninja files. So for example, if u need ninja runs in parallel u need specify it in GN. U work with ninja through GN. 



### Property of GN
* GN supports automatically re-running itself as needed by Ninja as part of the build. This eliminates the need to remember to re-run GN when you change a build file.
* GN gives us better tools for enforcing dependencies (see gn check and the visibility, public_deps, and data_deps options for some examples).s
* GN gives us tools for querying the build graph; you can ask “what does X depend on” and “who depends on Y”, for example.
* GN depends on Ninja (because generate ninja files). 
* It is designed for large projects and large teams. It scales efficiently to many thousands of build files and tens of thousands of source files.
* It is designed for multi-platform projects. It can cleanly express many complicated build variants across different platforms. A single build invocation can target multiple platforms.
* It supports multiple parallel output directories, each with their own configuration. This allows a developer to maintain builds targeting debug, release, or different platforms in parallel without forced rebuilds when switching.
* It has a focus on correctness. GN checks for the correct dependencies, inputs, and outputs to the extent possible, and has a number of tools to allow developers to ensure the build evolves as desired (for example, gn check, testonly, assert_no_deps).
* the exact way all compilers and linkers are run must be specified in the configuration (see “Examples” below). There is no default compiler configuration.
* Since GN is relatively uncommon, it can be more difficult to find information and examples.
* GN can generate Ninja build files for C, C++, Rust, Objective C, and Swift source on most popular platforms. We will talk about c++.
Other languages can be compiled using the general “action” rules which are executed by Python or another scripting language (Google does this to compile Java and Go).



### Installation
* GN binary
https://gn.googlesource.com/gn/#getting-a-binary
* Build gn from source code 
https://gn.googlesource.com/gn/#getting-a-binary




























