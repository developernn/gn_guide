# GN guide

This is a part of two-part guide. This about GN. 
Second one about ninja (link).


### Resources used for this guide
https://www.youtube.com/watch?v=QVZa7QbMix8&t=13s
https://www.topcoder.com/thrive/articles/Introduction%20to%20Build%20Tools%20GN%20&%20Ninja
https://gn.googlesource.com/gn/
https://docs.google.com/presentation/d/15Zwb53JcncHfEwHpnG_PoIbbzQ3GQi_cpujYwbpcbZo/edit#slide=id.g119d702868_0_12



### Usefull links
* Git repositories on gn
https://gn.googlesource.com/
* Repo where u can download gn
https://gn.googlesource.com/gn/
* GN documentation (only in repo)
https://gn.googlesource.com/gn/+/main/docs/



### Contribute GN
* Community and developers. The mailing list (https://groups.google.com/a/chromium.org/g/gn-dev).
* If you find a bug, you can see if it is known or report it in the bug database.(https://bugs.chromium.org/p/gn/issues/list).
https://gn.googlesource.com/
* Sending patches
https://gn.googlesource.com/gn/#sending-patches
























































